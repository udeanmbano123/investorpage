﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChartLibrary
{
    public class YearRevenue
    {
        #region Properties
        /// <summary>  
        /// get and set the PlayerId  
        /// </summary>  
        public int PlayerId
        {
            get;
            set;
        }
        /// <summary>  
        /// get and set the PlayerName  
        /// </summary>  
        public string PlayerName
        {
            get;
            set;
        }
        /// <summary>  
        /// get and set the PlayerList  
        /// </summary>  
        public List<Players> PlayerList
        {
            get;
            set;
        }
        /// <summary>  
        /// get and set the PlayerRecordList  
        /// </summary>  
        public List<PlayerRecord> PlayerRecordList
        {
            get;
            set;
        }#  
        endregion
    }
}
