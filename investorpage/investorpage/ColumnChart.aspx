﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ColumnChart.aspx.cs" Inherits="investorpage.ColumnChart" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Column Chart</title>
    <script src="Scripts/jquery-2.2.1.min.js"></script>
    <script src="http://www.google.com/jsapi" type="text/javascript"></script>
    <script type="text/javascript">
        // Global variable to hold data
        // Load the Visualization API and the piechart package.
        google.load('visualization', '1', { packages: ['corechart'] });</script>
    <script type="text/javascript">
        $(function () {
            $.ajax(
            {
                type: 'POST',
                dataType: 'json',
                contentType: 'application/json',
                url: 'ColumnChart.aspx/GetColumnChartData',
                data: '{}',
                success: function (response) {
                    drawchart(response.d); // calling method  
                },

                error: function () {
                    alert("Error loading data! Please try again.");
                }
            });
        })

        function drawchart(dataValues) {
            // Callback that creates and populates a data table,
            // instantiates the pie chart, passes in the data and
            // draws it.
            var data = new google.visualization.DataTable();
            data.addColumn('string', 'year');
            data.addColumn('number', 'Revenue');
          
            function getRandomColor() {
                var letters = '0123456789ABCDEF'.split('');
                var color = '#';
                for (var i = 0; i < dataValues.length; i++) {
                    color += letters[Math.floor(Math.random() * 16)];}
                return color;
            }
        
            for (var i = 0; i < dataValues.length; i++) 
            {data.addRow([dataValues[i].year, dataValues[i].Revenue,getRandomColor()]);
              
            }
            // Instantiate and draw our chart, passing in some options
            var options= {
                title: "Capital Expenditure",
                position: "top",
                fontsize: "14px",
          
                chartArea: { width: '50%' },
        
            };
           

            options.series = {};
            for (var i = 0; i < data.getNumberOfRows() ; i++) {
                options.series[i] = { color: getRandomColor() }
            }

           var chart =  new google.visualization.ColumnChart(document.getElementById('ColumnChartdiv'));

       chart.draw(data,options);
     
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <center>
             <div id="ColumnChartdiv" style="width:width: 400px; height: 350px;">
  
        </center>
     </div>
    </form>
</body>
</html>
