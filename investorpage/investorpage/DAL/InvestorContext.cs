﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Web;
using System.Web.Security;
using investorpage.Models;
using TrackerEnabledDbContext.Identity;


namespace investorpage.DAL
{
    public class InvestorContext : TrackerIdentityContext<ApplicationUser>
    {
        public InvestorContext() : base("InvestorContext")
        {
        }

        public DbSet<AddressChange> AddressChanges { get; set; }
        public DbSet<Administrator> Administrator { get; set; }
        public DbSet<ApprovalAccount> ApprovalAccounts { get; set; }
        public DbSet<BankingChange> BankingChanges { get; set; }
        public DbSet<Dividend> Dividends { get; set; }
        public DbSet<Event> Events { get; set; }

        public DbSet<Investor> Investors { get; set; }
        public DbSet<Performance> Performances { get; set; }
        public DbSet<Share> Shares { get; set; }
        public DbSet<ChairmanSt> ChairmanSt { get; set; }
        public DbSet<ImageGallery> ImageGallery { get; set; }
        public DbSet<Governance> Governances { get; set; }
        //financials
        public DbSet<Comprehensive_Income> ComprehensiveIncomes { get; set; }

        public DbSet<cashflows> cashflowss{ get; set; }
        public DbSet<ordinaryshare> ordinaryshares{ get; set; }
        public DbSet<other> others { get; set; }
        public DbSet<financial_position> financial_postions { get; set; }


        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            
            //modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
            //one-to-many relationships

            modelBuilder.Entity<ApprovalAccount>()
                .HasRequired(a => a.Investor)
                .WithMany()
                .HasForeignKey(u => u.InvestorID);

            /*  modelBuilder.Entity<User>()
                       .HasRequired(a => a.Administrator)
                       .WithMany()
                       .HasForeignKey(u => u.AdministratorID);
                       */
            modelBuilder.Entity<Share>()
                .HasRequired(a => a.Administrator)
                .WithMany()
                .HasForeignKey(u => u.AdministratorID);


            modelBuilder.Entity<User>()
                .HasRequired(a => a.Investor)
                .WithMany()
                .HasForeignKey(u => u.InvestorID);

            modelBuilder.Entity<User2>()
                        .HasRequired(a => a.Administrator)
                        .WithMany()
                        .HasForeignKey(u => u.AdministrationID);

            modelBuilder.Entity<User>()
                .HasMany(u => u.Roles)
                .WithMany(r => r.Users)
                .Map(m =>
                {
                    m.ToTable("UserRoles");
                    m.MapLeftKey("UserId");
                    m.MapRightKey("RoleId");
                });

            modelBuilder.Entity<User2>()
                .HasMany(u => u.Roles2)
                .WithMany(r => r.Users2)
                .Map(m =>
                {
                    m.ToTable("UserRoles2");
                    m.MapLeftKey("UserId");
                    m.MapRightKey("RoleId");
                });

            base.OnModelCreating(modelBuilder);
        }

    public DbSet<User> Users { get; set; }
        public DbSet<Role> Roles { get; set; }
        
        public DbSet<User2> Users2 { get; set; }
        public DbSet<Role2> Roles2 { get; set; }

      //  public System.Data.Entity.DbSet<investorpage.Models.ApplicationUser> ApplicationUsers { get; set; }
    }
  }
