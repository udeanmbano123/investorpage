﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace investorpage.Models
{public class  other
    {
        public int otherID { get; set; }
        [Required(ErrorMessage = "Net Asset Value is required")]
        [DisplayName("Net Asset")]
        public double net_asset_value { get; set; }
        [Required(ErrorMessage = "Current Ratio is required")]
        [DisplayName("Current Ratio")]
        public string current_ratio { get; set; }
        [Required(ErrorMessage = "Interest Cover is required")]
        [DisplayName("Interest Cover")]
        public double interest_cover { get; set; }
        [Required(ErrorMessage = "Number of Employees is required")]
        [DisplayName("Number Of Emplyees")]
        public int number_of_employees { get; set; }
        [Required(ErrorMessage = "Period Start Date is required")]
        [DataType(DataType.DateTime)]
        [DisplayName("Period Start Date")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy-MM-dd}")]
        public DateTime PeriodStartDate { get; set; }
        [Required(ErrorMessage = "Period End Date is required")]
        [DataType(DataType.DateTime)]
        [DisplayName("Period End Date")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy-MM-dd}")]
        public DateTime PeriodEndDate { get; set; }
        [Required(ErrorMessage = "Year is required")]
        [RegularExpression(@"^[0-9]*$", ErrorMessage = "Year is required only four digits")]
        [DisplayName("Year")]
        [StringLength(4, ErrorMessage = "Year should be four digits")]
        [MinLength(4)]
        public string year { get; set; }
    }
}
