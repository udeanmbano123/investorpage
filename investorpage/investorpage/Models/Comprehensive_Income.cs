﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace investorpage.Models
{
    public class Comprehensive_Income
    {
        public int Comprehensive_IncomeID { get; set; }
        [Required(ErrorMessage = "Revenue is required")]
        [DisplayName("Revenue")]
        public double Revenue { get; set; }
        [Required(ErrorMessage = "Operating Profit is required")]
        [DisplayName("Operating Profit")]
        public double operatingprofit { get; set; }
        [Required(ErrorMessage = "Net Interest is required")]
        [DisplayName("Net Interest")]
        public double netinterest { get; set; }
        [Required(ErrorMessage = "Loss or Profit is required")]
        [DisplayName("Loss Or Profit")]
        public double lossorprofit { get; set; }
        [Required(ErrorMessage = "Period Start Date is required")]
        [DataType(DataType.DateTime)]
        [DisplayName("Period Start Date")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy-MM-dd}")]
        public DateTime PeriodStartDate { get; set; }
        [Required(ErrorMessage = "Period End Date is required")]
        [DataType(DataType.DateTime)]
        [DisplayName("Period End Date")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy-MM-dd}")]
        public DateTime PeriodEndDate { get; set; }
        [Required(ErrorMessage = "Year is required")]
        [RegularExpression(@"^[0-9]*$", ErrorMessage = "Year is required only four digits")][DisplayName("Year")]
        [StringLength(4, ErrorMessage = "Year should be four digits")]
        [MinLength(4)]
        public string year { get; set; }
    }
}
