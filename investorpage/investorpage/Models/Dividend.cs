﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace investorpage.Models
{
    public class Dividend
    {
        public int DividendID { get; set; }
        [Required(ErrorMessage = "Dividend Type is required")]
        [DataType(DataType.Text)]
        [DisplayName("Dividend Cash Or Stock")]
        [StringLength(100)]
        public string DividendType { get; set; }
        [Required(ErrorMessage = "Announcement Date is required")]
        [DataType(DataType.DateTime)]
        [DisplayName("Announcement Date")]
        public DateTime AnnouncementDate { get; set; }
        [Required(ErrorMessage = "Ex-DividendDate is required")]
        [DataType(DataType.DateTime)]
        [DisplayName("ExDividendDate")]
        public DateTime ExDividendDate { get;set;}
        [Required(ErrorMessage = "Record Date is required")]
        [DataType(DataType.DateTime)]
        [DisplayName("Record Date")]
        public DateTime RecordDate { get; set; }
        [Required(ErrorMessage = "Payment Date is required")]
        [DataType(DataType.DateTime)]
        [DisplayName("Payment Date")]
        public DateTime PaymentDate{ get; set; }
        [Required(ErrorMessage = "Dividend Amount is required")]
        [DisplayName("Dividend Amount")]
        public double DividendAmount { get; set; }

        public int? User2ID { get; set; }

        [ForeignKey("User2ID")]
        public virtual User2 User2 { get; set; }

        public int? InvestorID { get; set; }

        [ForeignKey("InvestorID ")]
        public virtual Investor Investor { get; set; }
    }
}