﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace investorpage.Models
{
    public class BankingChange
    {
        public int BankingChangeID { get; set; }
        [Required(ErrorMessage = "Bank Name")]
        [DataType(DataType.Text)]
        [DisplayName("Bank")]
        public string BankName { get; set; }
        [Required(ErrorMessage = "Branch Name")]
        [DataType(DataType.Text)]
        [DisplayName("Branch Name")]
        public string BranchName { get; set; }
        [Required(ErrorMessage = "Branch Name")]
        [DataType(DataType.Text)]
        [DisplayName("Account Number")]
        public long AccountNumber { get; set; }
        [Required(ErrorMessage = "Reference Number is required")]
        [DataType(DataType.Text)]
        [DisplayName("Reference Number")]
        public string RefNumber { get; set; }
        [Required(ErrorMessage = "Application is required")]
        [DataType(DataType.DateTime)]
        [DisplayName("Applicaton Date")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy-MM-dd}")]
        public DateTime ApplicationDate { get; set; }
        [Required(ErrorMessage = "isAcknowledged is required")]
        [DisplayName("isAcknowledged")]
        public bool isAcknowledged { get; set; }
        [Required(ErrorMessage = "eMail is required")]
        [StringLength(35, ErrorMessage = "eMail Length Should be less than 35")]
        [RegularExpression(@"^[\w-]+(\.[\w-]+)*@([a-z0-9-]+(\.[a-z0-9-]+)*?\.[a-z]{2,6}|(\d{1,3}\.){3}\d{1,3})(:\d{4})?$", ErrorMessage = "eMail is not in proper format")]
        [DataType(DataType.EmailAddress)]
        [DisplayName("EmailAddress")]
        public string Email { get; set; }
      

        public int? UserID { get; set; }
        [ForeignKey("UserID ")]
        public virtual User User { get; set; }
    }
}