﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace investorpage.Models
{
    public class Governance
    {
        public int GovernanceID { get; set; }
        [DisplayName("Governance Header")]
        public string GovernanceHeader { get; set; }
        [Required(ErrorMessage = "Governance Section is required")]
        [DataType(DataType.MultilineText)]
        [DisplayName("Governance Section")]
        public string GovernanceSection { get; set; }
        [Required(ErrorMessage = " Date added is required")]
        [DataType(DataType.DateTime)]
        [DisplayName("Date Added")]
        public DateTime dateAdded { get; set; }
        [Required(ErrorMessage = "Year is required")]
        [DisplayName("GovernanaceYear")]
        public int Year { get; set; }
    }
}
