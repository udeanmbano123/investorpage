﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace investorpage.Models
{
    public class AddressChange
    {
        public int AddressChangeID { get; set; }
        [Required(ErrorMessage = "Full Address")]
        [DataType(DataType.Text)]
        [DisplayName("Address")]
        public string Address { get; set; }
        [Required(ErrorMessage = "City")]
        [DataType(DataType.Text)]
        [DisplayName("City")]
        public string City { get; set; }
        [Required(ErrorMessage = "Country")]
        [DataType(DataType.Text)]
        [DisplayName("Country")]
        public string Country { get; set; }
        [Required(ErrorMessage = "eMail is required")]
        [StringLength(35, ErrorMessage = "eMail Length Should be less than 35")]
        [RegularExpression(@"^[\w-]+(\.[\w-]+)*@([a-z0-9-]+(\.[a-z0-9-]+)*?\.[a-z]{2,6}|(\d{1,3}\.){3}\d{1,3})(:\d{4})?$", ErrorMessage = "eMail is not in proper format")]
        [DataType(DataType.EmailAddress)]
        [DisplayName("EmailAddress")]
        public string Email { get; set; }
        [Required(ErrorMessage = "Application Date is required")]
        [DataType(DataType.DateTime)]
        [DisplayName("Application Date")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy-MM-dd}")]
        public DateTime ApplicationDate { get; set; }
        [Required(ErrorMessage = "isAcknowledged is required")]
        [DisplayName("isAcknowledged")]
        public bool isAcknowledged{ get; set; }
       

        public int? UserID { get; set; }
        [ForeignKey("UserID ")]
        public virtual User User { get; set; }
    }
}