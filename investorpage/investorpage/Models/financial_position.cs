﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace investorpage.Models
{
    public class financial_position
    {
        public int financial_positionID { get; set; }
        [Required(ErrorMessage = "Equity attributable is required")]
        [DisplayName("Equity Attributable")]
        public double equityattributable { get; set; }
        [Required(ErrorMessage = "Interest Debt is required")]
        [DisplayName("Interest Debt")]
        public double interestdebt { get; set; }
        [Required(ErrorMessage = "Period Start Date is required")]
        [DataType(DataType.DateTime)]
        [DisplayName("Period Start Date")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy-MM-dd}")]
        public DateTime PeriodStartDate { get; set; }
        [Required(ErrorMessage = "Period End Date is required")]
        [DataType(DataType.DateTime)]
        [DisplayName("Period End Date")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy-MM-dd}")]
        public DateTime PeriodEndDate { get; set; }
        [Required(ErrorMessage = "Year is required")]
        [RegularExpression(@"^[0-9]*$", ErrorMessage = "Year is required only four digits")]
        [DisplayName("Year")]
        [StringLength(4, ErrorMessage = "Year should be four digits")]
        [MinLength(4)]
        public string year { get; set; }
    }
    
}
