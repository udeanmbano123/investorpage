﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace investorpage.Models
{
    public class Share
    {
        public int ShareID { get; set; }
        [Required(ErrorMessage = "Share Name is required")]
        [DataType(DataType.Text)]
        [DisplayName("Share Name")]
        public string ShareName { get; set; }
        //ISIN,Trading Status,Index,Sector,Year End,Chief Executive
        [Required(ErrorMessage = "ISIN is required")]
        [DataType(DataType.Text)]
        [DisplayName("ISN")]
        public string ISIN { get; set; }
        [Required(ErrorMessage = "Trading Status is required")]
        [DataType(DataType.Text)]
        [DisplayName("Trading Status")]
        public string TradingStatus { get; set; }
        [Required(ErrorMessage = "Index is required")]
        [DataType(DataType.Text)]
        [DisplayName("Index")]
        public string Index { get; set; }
        [Required(ErrorMessage = "Sector is required")]
        [DataType(DataType.Text)]
        [DisplayName("Sector")]
        public string Sector{ get; set; }
        [Required(ErrorMessage = "Year End is required")]
        [DataType(DataType.DateTime)]
        [DisplayName("Year End")]
        public DateTime YearEnd { get; set; }
        public double LastTrade { get; set; }
        public double changeonday { get; set; }
        public string ChiefExecutive { get; set; }

        public int? AdministratorID { get; set; }
        [ForeignKey("AdministratorID ")]
        public virtual Administrator Administrator { get; set; }

        public virtual ICollection<Performance> Performance { get; set; }
    }
}