﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace investorpage.Models
{
    public class Investor
    {
        public int InvestorID { get; set; }
        [Required(ErrorMessage = "Investor Type is required")]
        [DataType(DataType.Text)]
        [DisplayName("Investor Type")]
        [StringLength(100)]
        public string InvestorType { get; set; }
        [Required(ErrorMessage = "Investor Name is required")]
        [DataType(DataType.Text)]
        [DisplayName("Investor Name")]
        [StringLength(100)]
        public string InvestorName { get; set; }
        [Required(ErrorMessage = "Investor Name is required")]
        [DataType(DataType.Currency)]
        [DisplayName("Total Amount Invested")]
        public double TotalAmtInvested { get; set; }
        [Required(ErrorMessage = "Current Value")]
        [DataType(DataType.Currency)]
        [DisplayName("Investor Current Value")]
        public double CurrentValue { get; set; }
        [Required(ErrorMessage = "Bank Name")]
        [DataType(DataType.Text)]
        [DisplayName("Bank")]
        public string BankName { get; set; }
        [Required(ErrorMessage = "Branch Name")]
        [DataType(DataType.Text)]
        [DisplayName("Branch Name")]
        public string BranchName { get; set; }
        [Required(ErrorMessage = "Account Number")]
        [RegularExpression("^[0-9]{10}$", ErrorMessage = "Invalid Account No 10 digits")]
        [DisplayName("Account Number")]
        public long AccountNumber { get; set; }
        [Required(ErrorMessage = "Reference Number is required")]
        [DataType(DataType.Text)]
        [DisplayName("Reference Number")]
        public string RefNumber { get; set; }
        [Required(ErrorMessage = "Full Address")]
        [DataType(DataType.Text)]
        [DisplayName("Address")]
        public string Address { get; set; }
        [Required(ErrorMessage = "City")]
        [DataType(DataType.Text)]
        [DisplayName("City")]
        public string City { get; set; }
        [Required(ErrorMessage = "Country")]
        [DataType(DataType.Text)]
        [DisplayName("Country")]
        public string Country { get; set; }
        [Required(ErrorMessage = "Email")]
        [DataType(DataType.EmailAddress)]
        [DisplayName("Email")]
        public string Email { get; set; }
        [Required(ErrorMessage = "Added Date is required")]
        [DataType(DataType.DateTime)]
        [DisplayName("Added Date")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy-MM-dd}")]
        public DateTime AddedDate { get; set; }
        public User UserEmp { get; set; }

        public ApprovalAccount ApprovalAccount { get; set; }
        public virtual ICollection<Dividend> Dividend { get; set; }
        string _fullname;
        public string FullName
        {
            get { return _fullname; }
            set { _fullname = "Investor:" + InvestorName + " , " + "InvestorType" + " , " + InvestorType; }
        }
    }
}