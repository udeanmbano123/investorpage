﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace investorpage.Models
{
    public class Performance
    {
        public int PerformanceID { get; set; }
        [Required(ErrorMessage = "Bid is required")]
        [DisplayName("Bid")]
        public int Bid { get; set; }
        [Required(ErrorMessage = "Ask is required")]
        [DisplayName("Ask Amount")]
        public double Ask { get; set; }
        [Required(ErrorMessage = "Sales is required")]
        [DisplayName("Sales")]
        public double Sales { get; set; }
        [Required(ErrorMessage = "YTD is required")]
        [DisplayName("YTD% Amount")]
        public double YTD { get; set; }
        [Required(ErrorMessage = "YOY is required")]
        [DisplayName("YOY% Amount")]
        public double YOY { get; set; }

        public int? ShareID { get; set; }
        [ForeignKey("ShareID ")]
        public virtual Share Share { get; set; }
       
    }
}