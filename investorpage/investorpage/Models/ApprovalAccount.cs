﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace investorpage.Models
{
    public class ApprovalAccount
    {
        public int ApprovalAccountID { get; set; }
        [Required(ErrorMessage = "Approval Account Date is required")]
        [DataType(DataType.DateTime)]
        [DisplayName("Approval Account Date")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy-MM-dd}")]
        public DateTime ApprovalAccountDate { get; set; }
        [Required(ErrorMessage = "Investor Approved is required")]
        [DisplayName("InvestorApproved")]
        public bool isApproved { get; set; }
        public int? AdministratorID { get; set; }
        [Index("IX_InvestorID", IsUnique = true)]
        public int? InvestorID { get; set; }
        [ForeignKey("AdministratorID ")]
        public virtual Administrator Administrator { get; set; }

        [ForeignKey("InvestorID ")]
        public  Investor Investor { get; set; }
    }
}