﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace investorpage.Models
{
    public class cashflows
    {
        public int cashflowsID { get; set; }
        [Required(ErrorMessage = "Increase Cash is required")]
        [DisplayName("Increase Cash")]
        public double increaseincash { get; set; }
        [Required(ErrorMessage = "Net cash is required")]
        [DisplayName("Net Cash")]
        public double netcash { get; set; }
        [Required(ErrorMessage = "Period Start Date is required")]
        [DataType(DataType.DateTime)]
        [DisplayName("Period Start Date")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy-MM-dd}")]
        public DateTime PeriodStartDate { get; set; }
        [Required(ErrorMessage = "Period End Date is required")]
        [DataType(DataType.DateTime)]
        [DisplayName("Period End Date")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy-MM-dd}")]
        public DateTime PeriodEndDate { get; set; }
        [Required(ErrorMessage = "Year is required")]
        [RegularExpression(@"^[0-9]*$", ErrorMessage = "Year is required only four digits")]
        [DisplayName("Year")]
        [StringLength(4, ErrorMessage = "Year should be four digits")]
        [MinLength(4)]
        public string year { get; set; }
    }
}
