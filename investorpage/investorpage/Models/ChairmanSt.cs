﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace investorpage.Models
{
    public class ChairmanSt
    {
       
        public int ChairmanStID { get; set; }
        [Required(ErrorMessage = "Application Date is required")]
        [DataType(DataType.DateTime)]
        [DisplayName("Application Date")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy-MM-dd}")]
        public DateTime StatementDate { get; set; }
        [Required(ErrorMessage = "The Statement")]
        [DataType(DataType.MultilineText)]
        [DisplayName("Chairman Statement")]
        public string Statement { get; set; }
        [Required(ErrorMessage = "Year is required")]
        [DisplayName("Chairman Year")]
        [Index(IsUnique = true)]
        public int Year { get; set; }



    }
}
