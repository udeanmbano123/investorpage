﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace investorpage.Models
{
    public class ordinaryshare
    {
        public int ordinaryshareID { get; set; }
       [Required(ErrorMessage = "Basic Diluted is required")]
        [DisplayName("Basic Diluted")]
        public double basic_diluted { get; set; }
        [Required(ErrorMessage = "Loss per share is required")]
        [DisplayName("Loss Per Share")]
        public double loss_per_share { get; set; }
        [Required(ErrorMessage = "Market Price is required")]
        [DisplayName("Market Price")]
        public double market_price { get; set; }
        [Required(ErrorMessage = "Shraes In Issue is required")]
        [DisplayName("Share Issue")]
        public double shares_in_issue { get; set; }
        [Required(ErrorMessage = "Period Start Date is required")]
        [DataType(DataType.DateTime)]
        [DisplayName("Period Start Date")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy-MM-dd}")]
        public DateTime PeriodStartDate { get; set; }
        [Required(ErrorMessage = "Period End Date is required")]
        [DataType(DataType.DateTime)]
        [DisplayName("Period End Date")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy-MM-dd}")]
        public DateTime PeriodEndDate { get; set; }
        [Required(ErrorMessage = "Year is required")]
        [RegularExpression(@"^[0-9]*$", ErrorMessage = "Year is required only four digits")]
        [DisplayName("Year")]
        [StringLength(4, ErrorMessage = "Year should be four digits")]
        [MinLength(4)]
        public string year { get; set; }
    }
}
