﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using CompareObsolete = System.Web.Mvc.CompareAttribute;
using System.Web;

namespace investorpage.Models
{
    [TrackChanges]
    [Table("User2")]
    public class User2
    {
        public int User2ID { get; set; }




        [Required]
        public String Username { get; set; }



        [Required(ErrorMessage = "eMail is required")]
        [StringLength(35, ErrorMessage = "eMail Length Should be less than 35")]
        [RegularExpression(@"^\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$", ErrorMessage = "eMail is not in proper format")]
        [DataType(DataType.EmailAddress)]
        [DisplayName("EmailAddress")]
        [Index(IsUnique = true)]

        public String Email { get; set; }
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]

        [Display(Name = "Password")]
        [Required]
        public String Password { get; set; }


        [Display(Name = "Confirm password")]
        [Required]
        [CompareObsolete("Password", ErrorMessage = "The password and confirmation password do not match.")]

        public String ConfirmPassword { get; set; }

        [Required]
        [SkipTracking]
        public String FirstName { get; set; }

        [Required]
        [SkipTracking]
        public String LastName { get; set; }

        [SkipTracking]
        [Index("IX_AdministrationID", IsUnique = true)]
        public int? AdministrationID { get; set; }


        [SkipTracking]
        public Boolean IsActive { get; set; }
        [SkipTracking]
        public DateTime CreateDate { get; set; }

        [ForeignKey("AdministrationID ")]
        public Administrator Administrator { get; set; }
        public virtual ICollection<Role2> Roles2 { get; set; }

        public virtual ICollection<Dividend> Dividends { get; set; }
   

    }
}