﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace investorpage.Models
{
    public class Administrator
    {
        public int AdministratorID { get; set; }
        [Required(ErrorMessage = "First Name is required")]
        [DataType(DataType.Text)]
        [DisplayName(" First Name")]
        [StringLength(100)]
        public string FirstName { get; set; }
        [Required(ErrorMessage = "LastName is required")]
        [DataType(DataType.Text)]
        [DisplayName(" Last Name")]
        [StringLength(100)]
        public string LastName { get; set; }
        [Required(ErrorMessage = "eMail is required")]
        [StringLength(35, ErrorMessage = "eMail Length Should be less than 35")]
        [RegularExpression(@"^\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$", ErrorMessage = "eMail is not in proper format")]
        [DataType(DataType.EmailAddress)]
        [DisplayName("EmailAddress")]
        public string Email { get; set; }
        [Required(ErrorMessage = "Address is required")]
        [DataType(DataType.MultilineText)]
        [DisplayName("Address")]
        [StringLength(1024)]
        public string Address { get; set; }
        [Required(ErrorMessage = "City is required")]
        [DataType(DataType.Text)]
        [DisplayName("City")]
        [StringLength(100)]
        public string City { get; set; }
        [Required(ErrorMessage = "Country is required")]
        [DataType(DataType.Text)]
        [DisplayName("Country")]
        [StringLength(100)]
        public string Country { get; set; }
        [Required(ErrorMessage = "Position is required")]
        [DataType(DataType.Text)]
        [DisplayName("Position")]
        [StringLength(100)]
        public string Position { get; set; }
        public User2 User2 { get; set; }

        public virtual ICollection<Event> Event { get; set; }
     
        public virtual ICollection<ApprovalAccount> ApprovalAccount { get; set; }
        public virtual ICollection<Share> Shares { get; set; }

        
        string _fullname;
        [DisplayName("Administrator FullName")]
        public string FullName
        {
            get { return _fullname; }
            set { _fullname = FirstName + "," + LastName; }
        }
    }
}