﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace investorpage.Models
{
    public class Event
    {
        public int EventID { get; set; }
        public int? AdministratorID { get; set; }
        [Required(ErrorMessage = "Event Name is required")]
        [DataType(DataType.Text)]
        [DisplayName("Event Name")]
        [StringLength(100)]
        public string EventName { get; set; }
        [Required(ErrorMessage = "Description is required")]
        [DataType(DataType.MultilineText)]
        [StringLength(1024)]
        [DisplayName("Task Description")]
        public string Description { get; set; }
        [Required(ErrorMessage = "Start Date is required")]
        [DataType(DataType.DateTime)]
        [DisplayName("Start Date")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy-MM-dd}")]
        public DateTime StartDate { get; set; }
        [Required(ErrorMessage = "End Date is required")]
        [DataType(DataType.DateTime)]
        [DisplayName("End Date")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy-MM-dd}")]
        public DateTime EndDate { get; set; }

        [ForeignKey("AdministratorID ")]
        public virtual Administrator Administrator { get; set; }
       
    }
}