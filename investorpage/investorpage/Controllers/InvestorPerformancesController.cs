﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using investorpage.DAL;
using investorpage.DAL.security;
using investorpage.Models;

namespace investorpage.Controllers
{
    [CustomAuthorize(Roles = "User", NotifyUrl = "/UnauthorizedPage")]
    public class InvestorPerformancesController : Controller
    {
        private InvestorContext db = new InvestorContext();

        // GET: InvestorPerformances
        public ActionResult Index()
        {
            var performances = db.Performances.Include(p => p.Share);
            return View(performances.ToList());
        }

        // GET: InvestorPerformances/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Performance performance = db.Performances.Find(id);
            if (performance == null)
            {
                return HttpNotFound();
            }
            return View(performance);
        }

        // GET: InvestorPerformances/Create
        
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
