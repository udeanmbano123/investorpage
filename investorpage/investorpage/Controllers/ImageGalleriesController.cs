﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using investorpage.DAL;
using investorpage.Models;

namespace investorpage.Controllers
{
    public class ImageGalleriesController : Controller
    {
        private InvestorContext db = new InvestorContext();

        public ActionResult Index()
        {
            var imagesModel = new ImageGallery();
            var imageFiles = Directory.GetFiles(Server.MapPath("~/Upload_Files/"));
            foreach (var item in imageFiles)
            {
                imagesModel.ImageList.Add(Path.GetFileName(item));
            }
            return View(imagesModel);
        }
    
        [HttpGet]
        public ActionResult UploadImage()
        {
            return View();
        }
        [HttpPost]
        public ActionResult UploadImageMethod()
        {
            if (Request.Files.Count != 0)
            {
                for (int i = 0; i < Request.Files.Count; i++)
                {
                    HttpPostedFileBase file = Request.Files[i];
                    int fileSize = file.ContentLength;
                    string fileName = file.FileName;
                    try
                    {
                        file.SaveAs(Server.MapPath("~/Upload_Files/" + fileName));
                        ImageGallery imageGallery = new ImageGallery();
                        imageGallery.ID = Guid.NewGuid();
                        imageGallery.Name = fileName;
                        imageGallery.ImagePath = "~/Upload_Files/" + fileName;
                        db.ImageGallery.Add(imageGallery);
                        db.SaveChanges();
                    }
                    catch(Exception e)
                    {
                        return View("Index");
                    }
                }
                return Content("Success");
            }
            return Content("failed");
        }
    }
}