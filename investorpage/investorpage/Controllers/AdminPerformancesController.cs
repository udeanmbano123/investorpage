﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using investorpage.DAL;
using investorpage.Models;

namespace investorpage.Controllers
{
    public class AdminPerformancesController : Controller
    {
        private InvestorContext db = new InvestorContext();

        // GET: AdminPerformances
        public async Task<ActionResult> Index()
        {
            var performances = db.Performances.Include(p => p.Share);
            return View(await performances.ToListAsync());
        }

        // GET: AdminPerformances/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Performance performance = await db.Performances.FindAsync(id);
            if (performance == null)
            {
                return HttpNotFound();
            }
            return View(performance);
        }

        // GET: AdminPerformances/Create
        public ActionResult Create()
        {
            ViewBag.ShareID = new SelectList(db.Shares, "ShareID", "ShareName");
            return View();
        }

        // POST: AdminPerformances/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "PerformanceID,Bid,Ask,Sales,YTD,YOY,ShareID")] Performance performance)
        {
            if (ModelState.IsValid)
            {
                db.Performances.Add(performance);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            ViewBag.ShareID = new SelectList(db.Shares, "ShareID", "ShareName", performance.ShareID);
            return View(performance);
        }

        // GET: AdminPerformances/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Performance performance = await db.Performances.FindAsync(id);
            if (performance == null)
            {
                return HttpNotFound();
            }
            ViewBag.ShareID = new SelectList(db.Shares, "ShareID", "ShareName", performance.ShareID);
            return View(performance);
        }

        // POST: AdminPerformances/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "PerformanceID,Bid,Ask,Sales,YTD,YOY,ShareID")] Performance performance)
        {
            if (ModelState.IsValid)
            {
                db.Entry(performance).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.ShareID = new SelectList(db.Shares, "ShareID", "ShareName", performance.ShareID);
            return View(performance);
        }

        // GET: AdminPerformances/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Performance performance = await db.Performances.FindAsync(id);
            if (performance == null)
            {
                return HttpNotFound();
            }
            return View(performance);
        }

        // POST: AdminPerformances/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            Performance performance = await db.Performances.FindAsync(id);
            db.Performances.Remove(performance);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
