﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using investorpage.DAL;
using investorpage.Models;

namespace investorpage.Controllers
{
    public class ChairmanStsController : Controller
    {
        private InvestorContext db = new InvestorContext();

        // GET: ChairmanSts
        public async Task<ActionResult> Index()
        {
            return View(await db.ChairmanSt.ToListAsync());
        }

        // GET: ChairmanSts/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ChairmanSt chairmanSt = await db.ChairmanSt.FindAsync(id);
            if (chairmanSt == null)
            {
                return HttpNotFound();
            }
            return View(chairmanSt);
        }

        // GET: ChairmanSts/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: ChairmanSts/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "ChairmanStID,StatementDate,Statement,Year")] ChairmanSt chairmanSt)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    db.ChairmanSt.Add(chairmanSt);
                    await db.SaveChangesAsync();
                }
                catch (Exception e)
                {


                    var mod = ModelState.First(c => c.Key == "Year");  // this
                    mod.Value.Errors.Add("Duplicate Years are not allowed as the Chairman uses on statement per year");    // this
                    return View(chairmanSt);
                }

                return RedirectToAction("Index");
            }

            return View(chairmanSt);
        }

        // GET: ChairmanSts/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ChairmanSt chairmanSt = await db.ChairmanSt.FindAsync(id);
            if (chairmanSt == null)
            {
                return HttpNotFound();
            }
            return View(chairmanSt);
        }

        // POST: ChairmanSts/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "ChairmanStID,StatementDate,Statement,Year")] ChairmanSt chairmanSt)
        {
            if (ModelState.IsValid)
            {
                db.Entry(chairmanSt).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(chairmanSt);
        }

        // GET: ChairmanSts/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ChairmanSt chairmanSt = await db.ChairmanSt.FindAsync(id);
            if (chairmanSt == null)
            {
                return HttpNotFound();
            }
            return View(chairmanSt);
        }

        // POST: ChairmanSts/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            ChairmanSt chairmanSt = await db.ChairmanSt.FindAsync(id);
            db.ChairmanSt.Remove(chairmanSt);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
