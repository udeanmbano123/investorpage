﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.Mvc;
using System.Web.WebPages;
using investorpage.DAL;
using investorpage.DAL.security;
using investorpage.Models;
using PagedList;
namespace investorpage.Controllers
{
    [CustomAuthorize(Roles = "Secretary")]
    public class SecAddressChangesController : Controller
    {
        private InvestorContext db = new InvestorContext();

        // GET: SecAddressChanges
        public async Task<ActionResult> Index(string sortOrder, string currentFilter,string searchString, string Date1String, string Date2String,
            int? page)
        {
            ViewBag.CurrentSort = sortOrder;
            ViewBag.NameSortParm = String.IsNullOrEmpty(sortOrder) ? "name_desc" : "";
            ViewBag.CitySortParm = String.IsNullOrEmpty(sortOrder) ? "city" : "";
            ViewBag.DateSortParm = sortOrder == "Date" ? "date_desc" : "Date";
            ViewBag.EmailSortParm = String.IsNullOrEmpty(sortOrder) ? "email" : "";
            var addressChanges = db.AddressChanges.Include(a => a.User);
            if (searchString != null || (Date1String != null && Date2String != null))
            {
                page = 1;
            }
            else
            {
                searchString = currentFilter;
            }
            ViewBag.CurrentFilter = searchString;
          if (!String.IsNullOrEmpty(Date1String) && !String.IsNullOrEmpty(Date2String))
            {
                DateTime date = Convert.ToDateTime(Date1String);
                DateTime date2 = Convert.ToDateTime(Date2String);
                addressChanges = from s in db.AddressChanges.Include(a => a.User)
                                 where date <= s.ApplicationDate && date2 >= s.ApplicationDate
                            select s;


            }
            switch (sortOrder)
            {
                case "name_desc":
                    addressChanges =  addressChanges.OrderByDescending(s => s.AddressChangeID);
                    break;
                case "city":
                    addressChanges = addressChanges.OrderByDescending(s => s.City);
                    break;
                case "email":
                    addressChanges = addressChanges.OrderByDescending(s => s.Email);
                    break;
                case "Date":
                    addressChanges = addressChanges.OrderBy(s => s.ApplicationDate);
                    break;
                case "date_desc":
                    addressChanges = addressChanges.OrderByDescending(s => s.ApplicationDate);
                    break;
                default:
                    addressChanges = addressChanges.OrderBy(s => s.AddressChangeID);
                    break;
            }
            int pageSize = 5;
            int pageNumber = (page ?? 1);
            return View(addressChanges.ToPagedList(pageNumber, pageSize));
        }

        // GET: SecAddressChanges/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AddressChange addressChange = await db.AddressChanges.FindAsync(id);
            if (addressChange == null)
            {
                return HttpNotFound();
            }
            return View(addressChange);
        }

        // GET: SecAddressChanges/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AddressChange addressChange = await db.AddressChanges.FindAsync(id);
            if (addressChange == null)
            {
                return HttpNotFound();
            }
       
            ViewBag.UserID = new SelectList(db.Users, "UserID", "Username", addressChange.UserID);
            return View(addressChange);
        }

        // POST: SecAddressChanges/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "AddressChangeID,Address,City,Country,Email,isAcknowledged,ApplicationDate,UserID")] AddressChange addressChange)
        {
            if (ModelState.IsValid)
            {
               

                if (addressChange.isAcknowledged.Equals(true))
                {
                    db.Entry(addressChange).State = EntityState.Modified;
                    string messageBody = "Username :  " + Request["Email"] + " " +
                                         "your request has been acknowledged by the Secretary";
                    var body = "<p>Email From: {0} ({1})</p><p>Message:</p><p>{2}</p>";
                    var message = new MailMessage();
                    message.To.Add(new MailAddress(Request["Email"]));  // replace with valid value 
                    message.From = new MailAddress("sender@outlook.com");  // replace with valid value
                    message.Subject = "Address Change Acknowledgement";
                    message.Body = string.Format(body, "Admin", "udeanmbano@gmail.com", messageBody);
                    message.IsBodyHtml = true;

                    using (var smtp = new SmtpClient())
                    {
                        var credential = new NetworkCredential
                        {
                            UserName = "udeanmbano@gmail.com",  // replace with valid value
                            Password = "icomefromzimbabwe"  // replace with valid value
                        };
                        smtp.Credentials = credential;
                        smtp.Host = "smtp.gmail.com";
                        smtp.Port = 587;
                        smtp.EnableSsl = true;
                        await smtp.SendMailAsync(message);

                    }
                    db.Entry(addressChange).State = EntityState.Modified;
                    await db.SaveChangesAsync();
                }
                else
                {
                    db.Entry(addressChange).State = EntityState.Modified;
                    await db.SaveChangesAsync();
                }
                return RedirectToAction("Index");
            }

            ViewBag.UserID = new SelectList(db.Users, "UserID", "Username", addressChange.UserID);
            return View(addressChange);
        }

        // GET: SecAddressChanges/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AddressChange addressChange = await db.AddressChanges.FindAsync(id);
            if (addressChange == null)
            {
                return HttpNotFound();
            }
            return View(addressChange);
        }

        // POST: SecAddressChanges/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            AddressChange addressChange = await db.AddressChanges.FindAsync(id);
            db.AddressChanges.Remove(addressChange);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
