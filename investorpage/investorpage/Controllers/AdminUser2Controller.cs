﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.Mvc;
using investorpage.DAL;
using investorpage.Models;

namespace investorpage.Controllers
{
    public class AdminUser2Controller : Controller
    {
        private InvestorContext db = new InvestorContext();

        // GET: AdminUser2
        public async Task<ActionResult> Index()
        {
            var users2 = db.Users2.Include(u => u.Administrator);
            return View(await users2.ToListAsync());
        }

        // GET: AdminUser2/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            User2 user2 = await db.Users2.FindAsync(id);
            if (user2 == null)
            {
                return HttpNotFound();
            }
            return View(user2);
        }

        // GET: AdminUser2/Create
        public ActionResult Create()
        {
            ViewBag.AdministrationID = new SelectList(db.Administrator, "AdministratorID", "FullName");
            return View();
        }

        // POST: AdminUser2/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "User2ID,Username,Email,Password,ConfirmPassword,FirstName,LastName,AdministrationID,IsActive,CreateDate")] User2 user2)
        {
            if (ModelState.IsValid)
            {
                if (Request["Roles2"] == "Admin")
                {
                    Role2 role1 = new Role2 { RoleName = "Admin" };
                 
                    user2.Roles2 = new List<Role2>();
                    user2.Roles2.Add(role1);
                }
                else if (Request["Roles2"] == "Secretary")
                {
                    Role2 role2 = new Role2 { RoleName = "Secretary" };
                    user2.Roles2 = new List<Role2>();
                    user2.Roles2.Add(role2);
                }
                user2.Password = ComputeHash(user2.Password, new SHA256CryptoServiceProvider());
                user2.ConfirmPassword = ComputeHash(user2.ConfirmPassword, new SHA256CryptoServiceProvider());

                db.Users2.Add(user2);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            ViewBag.AdministrationID = new SelectList(db.Administrator, "AdministratorID", "FullName", user2.AdministrationID);
            return View(user2);
        }

        // GET: AdminUser2/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            User2 user2 = await db.Users2.FindAsync(id);
            user2.Password = "";
            user2.ConfirmPassword = "";
            if (user2 == null)
            {
                return HttpNotFound();
            }
            ViewBag.AdministrationID = new SelectList(db.Administrator, "AdministratorID", "FullName", user2.AdministrationID);
            return View(user2);
        }

        // POST: AdminUser2/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "User2ID,Username,Email,Password,ConfirmPassword,FirstName,LastName,AdministrationID,IsActive,CreateDate")] User2 user2)
        {
            if (ModelState.IsValid)
            {
                user2.Password = Request["Password"].ToString();
                user2.ConfirmPassword = Request["ConfirmPassword"].ToString();
                user2.Password = ComputeHash(user2.Password, new SHA256CryptoServiceProvider());
                user2.ConfirmPassword = ComputeHash(user2.ConfirmPassword, new SHA256CryptoServiceProvider());

                db.Entry(user2).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.AdministrationID = new SelectList(db.Administrator, "AdministratorID", "FullName", user2.AdministrationID);
            return View(user2);
        }

        // GET: AdminUser2/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            User2 user2 = await db.Users2.FindAsync(id);
            if (user2 == null)
            {
                return HttpNotFound();
            }
            return View(user2);
        }

        // POST: AdminUser2/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            User2 user2 = await db.Users2.FindAsync(id);
            db.Users2.Remove(user2);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
        //password  hashing function
        public string ComputeHash(string input, HashAlgorithm algorithm)
        {
            Byte[] inputBytes = Encoding.UTF8.GetBytes(input);

            Byte[] hashedBytes = algorithm.ComputeHash(inputBytes);

            return BitConverter.ToString(hashedBytes);
        }
    }
}
