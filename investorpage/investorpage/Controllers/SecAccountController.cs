﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.Mvc;
using investorpage.DAL;
using investorpage.Models;
using WebMatrix.WebData;

namespace investorpage.Controllers
{
    public class SecAccountController : Controller
    {
        private InvestorContext db = new InvestorContext();

        // GET: SecAccount
        public async Task<ActionResult> Index()
        {
            string name = WebSecurity.CurrentUserName;
            //to select the user who is currently logged in
            var user = from U in db.Users2
                       
                       where U.Email == name
                       select U;
            int? Logg = 0;
            //To assign logg to the value of the foreign key EmpID in table in User
            foreach (var key in user)
            {
                Logg = key.User2ID;
            }

           
            var users2 = db.Users2.Include(u => u.Administrator).Where(a=>a.User2ID==Logg);
            return View(await users2.ToListAsync());
        }

        // GET: SecAccount/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            User2 user2 = await db.Users2.FindAsync(id);
            if (user2 == null)
            {
                return HttpNotFound();
            }
            return View(user2);
        }

       
        // GET: SecAccount/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            User2 user2 = await db.Users2.FindAsync(id);
            user2.Password = "";
            user2.ConfirmPassword = "";
            if (user2 == null)
            {
                return HttpNotFound();
            }
            string name = WebSecurity.CurrentUserName;
            var userr = from U in db.Users2

                        where U.Email == name
                        select U;
            int? Logg = 0;
           // DateTime loggDate = new DateTime();
            //To assign logg to the value of the foreign key EmpID in table in User
            foreach (var key in userr)
            {
                Logg = key.User2ID;

            }
            var administrators = from I in db.Administrator
                            join U in db.Users2 on I.AdministratorID equals U.AdministrationID
                            where U.User2ID == Logg
                            select I;
            ViewBag.AdministrationID = new SelectList(administrators, "AdministratorID", "FullName", user2.AdministrationID);
            return View(user2);
        }

        // POST: SecAccount/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "User2ID,Username,Email,Password,ConfirmPassword,FirstName,LastName,AdministrationID,IsActive,CreateDate")] User2 user2)
        {
            


            if (ModelState.IsValid)
            {
                user2.Password = Request["Password"].ToString();
                user2.ConfirmPassword = Request["ConfirmPassword"].ToString();
                user2.Password = ComputeHash(user2.Password, new SHA256CryptoServiceProvider());
                user2.ConfirmPassword = ComputeHash(user2.ConfirmPassword, new SHA256CryptoServiceProvider());

                db.Entry(user2).State = EntityState.Modified;
                string name2 = WebSecurity.CurrentUserName;
                //to select the user who is currently logged in
                var user = from U in db.Users2

                           where U.Email == name2
                           select U;
                String Logg2 = "";
                //To assign logg to the value of the foreign key EmpID in table in User
                foreach (var key in user)
                {
                    Logg2 = key.Email;
                }
            
                await db.SaveChangesAsync(Logg2);
               
                return RedirectToAction("Index");
            }
            string name = WebSecurity.CurrentUserName;
            var userr = from U in db.Users2

                        where U.Email == name
                        select U;
            int? Logg = 0;
            
            //To assign logg to the value of the foreign key EmpID in table in User
            foreach (var key in userr)
            {
                Logg = key.User2ID;

            }
            var administrators = from I in db.Administrator
                                 join U in db.Users2 on I.AdministratorID equals U.AdministrationID
                                 where U.User2ID == Logg
                                 select I;
            ViewBag.AdministrationID = new SelectList(administrators, "AdministratorID", "FullName", user2.AdministrationID);
            return View(user2);
        }

        // GET: SecAccount/Delete/5
     

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
        //password  hashing function
        public string ComputeHash(string input, HashAlgorithm algorithm)
        {
            Byte[] inputBytes = Encoding.UTF8.GetBytes(input);

            Byte[] hashedBytes = algorithm.ComputeHash(inputBytes);

            return BitConverter.ToString(hashedBytes);
        }
    }
}
