﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.Mvc;
using investorpage.DAL;
using investorpage.DAL.security;
using investorpage.Models;
using PagedList;

namespace investorpage.Controllers
{
    [CustomAuthorize(Roles = "Secretary")]
    public class SecBankChangesController : Controller
    {
        private InvestorContext db = new InvestorContext();

        // GET: SecBankChanges
        public async Task<ActionResult> Index(string sortOrder, string currentFilter, string searchString, string Date1String, string Date2String,
            int? page)
        {ViewBag.CurrentSort = sortOrder;
            ViewBag.NameSortParm = String.IsNullOrEmpty(sortOrder) ? "name_desc" : "";
            ViewBag.BankSortParm = String.IsNullOrEmpty(sortOrder) ? "Bank" : "";
            ViewBag.DateSortParm = sortOrder == "Date" ? "date_desc" : "Date";
            ViewBag.EmailSortParm = String.IsNullOrEmpty(sortOrder) ? "email" : "";
            var bankingChanges = db.BankingChanges.Include(b => b.User);
            if (searchString != null || (Date1String != null && Date2String != null))
            {
                page = 1;
            }
            else
            {
                searchString = currentFilter;
            }
            ViewBag.CurrentFilter = searchString;
          if (!String.IsNullOrEmpty(Date1String) && !String.IsNullOrEmpty(Date2String))
            {
                DateTime date = Convert.ToDateTime(Date1String);
                DateTime date2 = Convert.ToDateTime(Date2String);
                bankingChanges = from s in db.BankingChanges.Include(b => b.User)
                                 where date <= s.ApplicationDate && date2 >= s.ApplicationDate
                            select s;


            }
            switch (sortOrder)
            {
                case "name_desc":
                    bankingChanges = bankingChanges.OrderByDescending(s => s.BankingChangeID);
                    break;
                case "Bank":
                    bankingChanges = bankingChanges.OrderByDescending(s => s.BankName);
                    break;
                case "email":
                    bankingChanges = bankingChanges.OrderByDescending(s => s.Email);
                    break;
                case "Date":
                    bankingChanges = bankingChanges.OrderBy(s => s.ApplicationDate);
                    break;
                case "date_desc":
                    bankingChanges = bankingChanges.OrderByDescending(s => s.ApplicationDate);
                    break;
                default:
                    bankingChanges = bankingChanges.OrderBy(s => s.BankingChangeID);
                    break;
            }
            int pageSize = 5;
            int pageNumber = (page ?? 1);

            return View(bankingChanges.ToPagedList(pageNumber, pageSize));
        }

        // GET: SecBankChanges/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BankingChange bankingChange = await db.BankingChanges.FindAsync(id);
            if (bankingChange == null)
            {
                return HttpNotFound();
            }
            return View(bankingChange);
        }

       
        // GET: SecBankChanges/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BankingChange bankingChange = await db.BankingChanges.FindAsync(id);
            if (bankingChange == null)
            {
                return HttpNotFound();
            }
           
            ViewBag.UserID = new SelectList(db.Users, "UserID", "Username", bankingChange.UserID);
            return View(bankingChange);
        }

        // POST: SecBankChanges/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "BankingChangeID,BankName,BranchName,AccountNumber,RefNumber,ApplicationDate,isAcknowledged,Email,UserID")] BankingChange bankingChange)
        {
            if (ModelState.IsValid)
            {

                if (bankingChange.isAcknowledged.Equals(true))
                {
                    db.Entry(bankingChange).State = EntityState.Modified;
                    string messageBody = "Username :  " + Request["Email"] +" "+
                                         "your request has been acknowledged by the Secretary";
                    var body = "<p>Email From: {0} ({1})</p><p>Message:</p><p>{2}</p>";
                    var message = new MailMessage();
                    message.To.Add(new MailAddress(Request["Email"]));  // replace with valid value 
                    message.From = new MailAddress("sender@outlook.com");  // replace with valid value
                    message.Subject = "Banking Change Acknowledgement";
                    message.Body = string.Format(body, "Admin", "udeanmbano@gmail.com", messageBody);
                    message.IsBodyHtml = true;

                    using (var smtp = new SmtpClient())
                    {
                        var credential = new NetworkCredential
                        {
                            UserName = "udeanmbano@gmail.com",  // replace with valid value
                            Password = "icomefromzimbabwe"  // replace with valid value
                        };
                        smtp.Credentials = credential;
                        smtp.Host = "smtp.gmail.com";
                        smtp.Port = 587;
                        smtp.EnableSsl = true;
                        await smtp.SendMailAsync(message);

                    }
                    db.Entry(bankingChange).State = EntityState.Modified;
                    await db.SaveChangesAsync();
                }
                else
                {
                    db.Entry(bankingChange).State = EntityState.Modified;
                    await db.SaveChangesAsync();
                }
                return RedirectToAction("Index");
            }
            
            ViewBag.UserID = new SelectList(db.Users, "UserID", "Username", bankingChange.UserID);
            return View(bankingChange);
        }

        // GET: SecBankChanges/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BankingChange bankingChange = await db.BankingChanges.FindAsync(id);
            if (bankingChange == null)
            {
                return HttpNotFound();
            }
            return View(bankingChange);
        }

        // POST: SecBankChanges/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            BankingChange bankingChange = await db.BankingChanges.FindAsync(id);
            db.BankingChanges.Remove(bankingChange);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
