﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using investorpage.DAL;
using investorpage.DAL.security;
using investorpage.Models;

namespace investorpage.Controllers
{
    [CustomAuthorize(Roles = "Admin")]
    public class ApprovalAccountsController : Controller
    {
        private InvestorContext db = new InvestorContext();

        // GET: ApprovalAccounts
        public ActionResult Index()
        {

            var approvalAccounts = db.ApprovalAccounts.Include(a => a.Administrator).Include(a => a.Investor);
            return View(approvalAccounts.ToList());
        }

        // GET: ApprovalAccounts/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ApprovalAccount approvalAccount = db.ApprovalAccounts.Find(id);
            if (approvalAccount == null)
            {
                return HttpNotFound();
            }
            return View(approvalAccount);
        }

        // GET: ApprovalAccounts/Create
        public ActionResult Create()
        {
            ViewBag.AdministratorID = new SelectList(db.Administrator, "AdministratorID", "FullName");
            var investors = (from tr in db.Investors
                             where !db.ApprovalAccounts.Any(dm => tr.InvestorID == dm.InvestorID)
                             select tr).ToList();
            
            ViewBag.InvestorID = new SelectList(investors, "InvestorID", "FullName");
            return View();
        }

        // POST: ApprovalAccounts/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ApprovalAccountID,ApprovalAccountDate,isApproved,AdministratorID,InvestorID")] ApprovalAccount approvalAccount)
        {
            if (ModelState.IsValid)
            {
                db.ApprovalAccounts.Add(approvalAccount);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.AdministratorID = new SelectList(db.Administrator, "AdministratorID", "FullName", approvalAccount.AdministratorID);
            var investors = (from tr in db.Investors
                             where !db.ApprovalAccounts.Any(dm => tr.InvestorID == dm.InvestorID)
                             select tr).ToList();
            ViewBag.InvestorID = new SelectList(investors, "InvestorID", "FullName", approvalAccount.InvestorID);
            return View(approvalAccount);
        }

        // GET: ApprovalAccounts/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ApprovalAccount approvalAccount = db.ApprovalAccounts.Find(id);
            if (approvalAccount == null)
            {
                return HttpNotFound();
            }
            ViewBag.AdministratorID = new SelectList(db.Administrator, "AdministratorID", "FullName", approvalAccount.AdministratorID);
            var investors = from tr in db.Investors
              
                select tr;
            ViewBag.InvestorID = new SelectList(investors, "InvestorID", "FullName", approvalAccount.InvestorID);
            return View(approvalAccount);
        }

        // POST: ApprovalAccounts/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ApprovalAccountID,ApprovalAccountDate,isApproved,AdministratorID,InvestorID")] ApprovalAccount approvalAccount)
        {
            if (ModelState.IsValid)
            {
                db.Entry(approvalAccount).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.AdministratorID = new SelectList(db.Administrator, "AdministratorID", "FullName", approvalAccount.AdministratorID);
            var investors = from tr in db.Investors

                            select tr;
            ViewBag.InvestorID = new SelectList(investors, "InvestorID", "FullName", approvalAccount.InvestorID);
            return View(approvalAccount);
        }

        // GET: ApprovalAccounts/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ApprovalAccount approvalAccount = db.ApprovalAccounts.Find(id);
            if (approvalAccount == null)
            {
                return HttpNotFound();
            }
            return View(approvalAccount);
        }

        // POST: ApprovalAccounts/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            ApprovalAccount approvalAccount = db.ApprovalAccounts.Find(id);
            db.ApprovalAccounts.Remove(approvalAccount);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
