﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using investorpage.DAL;
using investorpage.DAL.security;
using investorpage.Models;
using WebMatrix.WebData;
namespace investorpage.Controllers
{
    [CustomAuthorize(Roles = "User", NotifyUrl = "/UnauthorizedPage")]
    public class InvestorAddressChangesController : Controller
    {
        private InvestorContext db = new InvestorContext();

        // GET: InvestorAddressChanges
        public ActionResult Index()
        {
            string name = WebSecurity.CurrentUserName;
            var user = from U in db.Users

                       where U.Email == name
                       select U;
            int? Logg = 0;
            //To assign logg to the value of the foreign key EmpID in table in User
            foreach (var key in user)
            {
                Logg = key.UserID;
            }

            var addressChanges = db.AddressChanges.Include(a => a.User).Where(a=>a.UserID==Logg);
            return View(addressChanges.ToList());
        }

        // GET: InvestorAddressChanges/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AddressChange addressChange = db.AddressChanges.Find(id);
            if (addressChange == null)
            {
                return HttpNotFound();
            }
            return View(addressChange);
        }

        // GET: InvestorAddressChanges/Create
        public ActionResult Create()
        {
          
            string name = WebSecurity.CurrentUserName;
            var user = from U in db.Users

                       where U.Email == name
                       select U;
            int? Logg = 0;
            //To assign logg to the value of the foreign key EmpID in table in User
            foreach (var key in user)
            {
                Logg = key.UserID;
            }
            var users = from U in db.Users
                        where U.UserID == Logg
                        select U;
            ViewBag.UserID = new SelectList(users, "UserID", "Username");
            return View();
        }

        // POST: InvestorAddressChanges/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "AddressChangeID,Address,City,Country,Email,isAcknowledged,ApplicationDate,UserID")] AddressChange addressChange)
        {
            if (ModelState.IsValid)
            {
                addressChange.isAcknowledged = false;
               
                db.AddressChanges.Add(addressChange);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
           
            string name = WebSecurity.CurrentUserName;
            var user = from U in db.Users

                       where U.Email == name
                       select U;
            int? Logg = 0;
            //To assign logg to the value of the foreign key EmpID in table in User
            foreach (var key in user)
            {
                Logg = key.UserID;
            }
            var users = from U in db.Users
                        where U.UserID == Logg
                        select U;
            ViewBag.UserID = new SelectList(users, "UserID", "Username", addressChange.UserID);
            return View(addressChange);
        }

       
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
