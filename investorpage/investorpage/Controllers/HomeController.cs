﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using investorpage.DAL;
using investorpage.Models;
using PagedList;

namespace investorpage.Controllers
{
    public class HomeController : Controller
    {
        private InvestorContext db = new InvestorContext();
        public ActionResult Index()
        {
            ViewBag.Message = "Investor Page";
            return View("Index");
        }

  

        public ActionResult FinancialReporting()
        {
           // ViewBag.Title = "Financial Reporting";
            ViewBag.Message = "Financial Reporting";

            return View();
        }

        public ActionResult Gallery()
        {
            var imagesModel = new ImageGallery();
            var imageFiles = Directory.GetFiles(Server.MapPath("~/Upload_Files/"));
            foreach (var item in imageFiles)
            {
                imagesModel.ImageList.Add(Path.GetFileName(item));
            }
            return View(imagesModel);
        }
    
      


   
        public async Task<ActionResult> Governance(string sortOrder, string currentFilter, string
searchString, int? page)
        { 

            ViewBag.CurrentSort = sortOrder;
ViewBag.NameSortParm = String.IsNullOrEmpty(sortOrder) ? "name_desc" : "";
ViewBag.DateSortParm = sortOrder == "Date" ? "date_desc" : "Date";
if (searchString != null)
        {
        
            page = 1;
        }
     else
        {
searchString = currentFilter;
                   }
    ViewBag.CurrentFilter = searchString;
            var Gv = from s in db.Governances
                     where s.Year==DateTime.Now.Year
                     select s;
            if (!String.IsNullOrEmpty(searchString))
            {
               
            }
            switch (sortOrder)
            {
                case "name_desc":
                    Gv = Gv.OrderByDescending(s => s.GovernanceHeader);
                    break;
                case "Date":
                   Gv= Gv.OrderBy(s => s.dateAdded);
                    break;
                case "date_desc":
                    Gv = Gv.OrderByDescending(s => s.dateAdded);
                    break;
                default: // Name ascending
                    Gv = Gv.OrderBy(s => s.GovernanceID);
                    break;
            }
            int pageSize = 3;
            int pageNumber = (page ?? 1);

            //for file download
       
            string[] files = Directory.GetFiles(Server.MapPath("~/App_Data/uploads"));
            for (int i = 0; i < files.Length; i++)
            {
                files[i] = Path.GetFileName(files[i]);
            }
            ViewBag.Files = files;
            ViewBag.Title = "Governance Principles";
            return View(Gv.ToPagedList(pageNumber, pageSize));
        }
 

        

        public ActionResult FinancialHighLights()
        {
            ViewBag.Message = "Financial HighLights";

            return View();

        }

    
        public ActionResult Chairman()
        {
           // ViewBag.Title = "Chairman";
            ViewBag.Message = "Chairman";

            return View();
        }

        public ActionResult Partial()
        {
            return PartialView();
        }
        public ActionResult Partial2()
        {
            return PartialView();
        }
        public ActionResult Partial3()
        {
            return PartialView();
        }
        public ActionResult Partial4()
        {
            return PartialView();
        }
        public ActionResult PartialView5()
        {
            return PartialView();
        }
    
     
 
        ///
        ///  
      
public FileResult DownloadFile(string fileName)
{
    var filepath = System.IO.Path.Combine(Server.MapPath("~/App_Data/uploads"), fileName);
    return File(filepath, MimeMapping.GetMimeMapping(filepath), fileName);
}
    }
}