﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using investorpage.DAL;
using investorpage.Models;
using WebMatrix.WebData;

namespace investorpage.Controllers
{
    public class InvestorDividendsController : Controller
    {
        private InvestorContext db = new InvestorContext();

        // GET: InvestorDividends
        public async Task<ActionResult> Index()
        {
            string name = WebSecurity.CurrentUserName;
            var user = from U in db.Users

                       where U.Email == name
                       select U;
            int? Logg = 0;
            //To assign logg to the value of the foreign key EmpID in table in User
            foreach (var key in user)
            {
                Logg = key.InvestorID;
            }
            var dividends = db.Dividends.Include(d => d.Investor).Include(d => d.User2).Where(d=>d.InvestorID==Logg);
            return View(await dividends.ToListAsync());
        }

        // GET: InvestorDividends/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Dividend dividend = await db.Dividends.FindAsync(id);
            if (dividend == null)
            {
                return HttpNotFound();
            }
            return View(dividend);
        }



        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
