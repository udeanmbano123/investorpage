﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using investorpage.DAL;
using investorpage.DAL.security;
using investorpage.Models;
using WebMatrix.WebData;
namespace investorpage.Controllers
{
    [CustomAuthorize(Roles = "User", NotifyUrl = "/UnauthorizedPage")]
    public class InvestorBankingChangesController : Controller
    {
        private InvestorContext db = new InvestorContext();

        // GET: InvestorBankingChanges
        public ActionResult Index()
        {
            string name = WebSecurity.CurrentUserName;
            var user = from U in db.Users

                       where U.Email == name
                       select U;
            int? Logg = 0;
            //To assign logg to the value of the foreign key EmpID in table in User
            foreach (var key in user)
            {
                Logg = key.UserID;
            }
            var bankingChanges = db.BankingChanges.Include(b => b.User).Where(b=>b.UserID==Logg);
            return View(bankingChanges.ToList());
        }

        // GET: InvestorBankingChanges/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BankingChange bankingChange = db.BankingChanges.Find(id);
            if (bankingChange == null)
            {
                return HttpNotFound();
            }
            return View(bankingChange);
        }

        // GET: InvestorBankingChanges/Create
        public ActionResult Create()
        {
            
            string name = WebSecurity.CurrentUserName;
            var user = from U in db.Users

                       where U.Email == name
                       select U;
            int? Logg = 0;
            //To assign logg to the value of the foreign key EmpID in table in User
            foreach (var key in user)
            {
                Logg = key.UserID;
            }
            var users = from U in db.Users
                        where U.UserID == Logg
                        select U;
            ViewBag.UserID = new SelectList(users, "UserID", "Username");
            return View();
        }

        // POST: InvestorBankingChanges/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "BankingChangeID,BankName,BranchName,AccountNumber,RefNumber,ApplicationDate,isAcknowledged,UserID")] BankingChange bankingChange)
        {
            if (ModelState.IsValid)
            {
                bankingChange.isAcknowledged = false;
                
                db.BankingChanges.Add(bankingChange);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
      
            string name = WebSecurity.CurrentUserName;
            var user = from U in db.Users

                       where U.Email == name
                       select U;
            int? Logg = 0;
            //To assign logg to the value of the foreign key EmpID in table in User
            foreach (var key in user)
            {
                Logg = key.UserID;
            }
            var users = from U in db.Users
                        where U.UserID == Logg
                        select U;
            ViewBag.UserID = new SelectList(users, "UserID", "Username", bankingChange.UserID);
            return View(bankingChange);
        }

        
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
