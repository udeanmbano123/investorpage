﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.Mvc;
using investorpage.DAL;
using investorpage.DAL.security;
using investorpage.Models;
using WebMatrix.WebData;

namespace investorpage.Controllers
{ [CustomAuthorize(Roles = "User", NotifyUrl = "/UnauthorizedPage")]
    public class UserAccountController : Controller
    {
       
        private InvestorContext db = new InvestorContext();

        // GET: UserAccount
        public async Task<ActionResult> Index()
        {
            string name = WebSecurity.CurrentUserName;
            var user = from U in db.Users

                       where U.Email == name
                       select U;
            int? Logg = 0;
            //To assign logg to the value of the foreign key EmpID in table in User
            foreach (var key in user)
            {
                Logg = key.UserID;
            }
            var users = db.Users.Include(u => u.Investor).Where(a=>a.UserID==Logg);
            return View(await users.ToListAsync());
        }

        // GET: UserAccount/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            User user = await db.Users.FindAsync(id);
            if (user == null)
            {
                return HttpNotFound();
            }
            return View(user);
        }

       

        // GET: UserAccount/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            User user = await db.Users.FindAsync(id);
            user.Password = "";
            user.ConfirmPassword = "";
            if (user == null)
            {
                return HttpNotFound();
            }
            string name = WebSecurity.CurrentUserName;
            var userr = from U in db.Users

                       where U.Email == name
                       select U;
            int? Logg = 0;
            DateTime loggDate=new DateTime();
            //To assign logg to the value of the foreign key EmpID in table in User
            foreach (var key in userr)
            {
                Logg = key.UserID;
                loggDate = key.CreateDate;
            }
            var investors = from I in db.Investors
                join U in db.Users on I.InvestorID equals U.InvestorID
                where U.UserID == Logg
                select I;
            ViewBag.InvestorID = new SelectList(investors, "InvestorID", "FullName", user.InvestorID);
            return View(user);
        }

        // POST: UserAccount/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "UserID,Username,Email,Password,ConfirmPassword,FirstName,LastName,InvestorID,IsActive,CreateDate")] User user)
        {
           

            if (ModelState.IsValid)
            {
                user.Password = Request["Password"].ToString();
                user.ConfirmPassword = Request["ConfirmPassword"].ToString();


                user.Password = ComputeHash(user.Password, new SHA256CryptoServiceProvider());
                user.ConfirmPassword = ComputeHash(user.ConfirmPassword, new SHA256CryptoServiceProvider());

                db.Entry(user).State = EntityState.Modified;
                    await db.SaveChangesAsync();
                
            
                return RedirectToAction("Index");
            }
            string name = WebSecurity.CurrentUserName;
            var userr = from U in db.Users

                        where U.Email == name
                        select U;
            int? Logg = 0;
            //To assign logg to the value of the foreign key EmpID in table in User
            foreach (var key in userr)
            {
                Logg = key.UserID;
            }
            var investors = from I in db.Investors
                            join U in db.Users on I.InvestorID equals U.InvestorID
                            where U.UserID == Logg
                            select I;
            ViewBag.InvestorID = new SelectList(investors, "InvestorID", "InvestorType", user.InvestorID);
            return View(user);
        }

       

    

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
        //password  hashing function
        public string ComputeHash(string input, HashAlgorithm algorithm)
        {
            Byte[] inputBytes = Encoding.UTF8.GetBytes(input);

            Byte[] hashedBytes = algorithm.ComputeHash(inputBytes);

            return BitConverter.ToString(hashedBytes);
        }
    }
}
