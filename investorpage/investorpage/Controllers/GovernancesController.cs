﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using investorpage.DAL;
using investorpage.Models;

namespace investorpage.Controllers
{
    public class GovernancesController : Controller
    {
        private InvestorContext db = new InvestorContext();

        // GET: Governances
        public async Task<ActionResult> Index()
        {
            return View(await db.Governances.ToListAsync());
        }

        // GET: Governances/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Governance governance = await db.Governances.FindAsync(id);
            if (governance == null)
            {
                return HttpNotFound();
            }
            return View(governance);
        }

        // GET: Governances/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Governances/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "GovernanceID,GovernanceHeader,GovernanceSection,dateAdded,Year")] Governance governance)
        {
            if (ModelState.IsValid)
            {
                db.Governances.Add(governance);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(governance);
        }

        // GET: Governances/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Governance governance = await db.Governances.FindAsync(id);
            if (governance == null)
            {
                return HttpNotFound();
            }
            return View(governance);
        }

        // POST: Governances/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "GovernanceID,GovernanceHeader,GovernanceSection,dateAdded,Year")] Governance governance)
        {
            if (ModelState.IsValid)
            {
                db.Entry(governance).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(governance);
        }

        // GET: Governances/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Governance governance = await db.Governances.FindAsync(id);
            if (governance == null)
            {
                return HttpNotFound();
            }
            return View(governance);
        }

        // POST: Governances/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            Governance governance = await db.Governances.FindAsync(id);
            db.Governances.Remove(governance);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
