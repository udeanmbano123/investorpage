﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using investorpage.DAL;
using investorpage.DAL.security;
using investorpage.Models;
using PagedList;

namespace investorpage.Controllers
{
    [CustomAuthorize(Roles = "Admin")]
    public class InvestorsController : Controller
    {
        private InvestorContext db = new InvestorContext();

        // GET: Investors
        public ActionResult Index(string sortOrder, string currentFilter, string searchString, string Date1String, string Date2String,
            int? page)
        {
            ViewBag.CurrentSort = sortOrder;
            ViewBag.NameSortParm = String.IsNullOrEmpty(sortOrder) ? "name_desc" : "";
            ViewBag.CurrentSortParm = String.IsNullOrEmpty(sortOrder) ? "current" : "";
            ViewBag.ToalSortParm = String.IsNullOrEmpty(sortOrder) ? "total" : "";
            ViewBag.DateSortParm = sortOrder == "Date" ? "date_desc" : "Date";
            ViewBag.EmailSortParm = String.IsNullOrEmpty(sortOrder) ? "email" : "";
            var investors = from s in db.Investors
                            select s;
            if (searchString != null || (Date1String != null && Date2String != null))
            {
                page = 1;
            }
            else
            {
                searchString = currentFilter;
            }
            ViewBag.CurrentFilter = searchString;
            if (!String.IsNullOrEmpty(Date1String) && !String.IsNullOrEmpty(Date2String))
            {
                DateTime date = Convert.ToDateTime(Date1String);
                DateTime date2 = Convert.ToDateTime(Date2String);
               investors= from s in db.Investors
                          where date <= s.AddedDate && date2 >= s.AddedDate
                          select s;


            }
            switch (sortOrder)
            {
                case "name_desc":
                    investors = investors.OrderByDescending(s => s.InvestorName);
                    break;
                case "total":
                    investors = investors.OrderByDescending(s => s.TotalAmtInvested);
                    break;
                case "current":
                    investors = investors.OrderByDescending(s => s.CurrentValue);
                    break;
                case "email":
                    investors = investors.OrderByDescending(s => s.Email);
                    break;
                case "Date":
                    investors = investors.OrderBy(s => s.AddedDate);
                    break;
                case "date_desc":
                    investors = investors.OrderByDescending(s => s.AddedDate);
                    break;
                default:
                    investors = investors.OrderBy(s => s.InvestorID);
                    break;
            }
            int pageSize = 5;
            int pageNumber = (page ?? 1);

            return View(investors.ToPagedList(pageNumber, pageSize));
        }

        // GET: Investors/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Investor investor = db.Investors.Find(id);
            if (investor == null)
            {
                return HttpNotFound();
            }
            return View(investor);
        }

        // GET: Investors/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Investors/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "InvestorID,InvestorType,InvestorName,TotalAmtInvested,CurrentValue,BankName,BranchName,AccountNumber,RefNumber,Address,City,Country,Email,AddedDate")] Investor investor)
        {
            if (ModelState.IsValid)
            {
                db.Investors.Add(investor);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(investor);
        }

        // GET: Investors/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Investor investor = db.Investors.Find(id);
            if (investor == null)
            {
                return HttpNotFound();
            }
            return View(investor);
        }

        // POST: Investors/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "InvestorID,InvestorType,InvestorName,TotalAmtInvested,CurrentValue,BankName,BranchName,AccountNumber,RefNumber,Address,City,Country,Email,AddedDate")] Investor investor)
        {
            if (ModelState.IsValid)
            {
                db.Entry(investor).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(investor);
        }

        // GET: Investors/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Investor investor = db.Investors.Find(id);
            if (investor == null)
            {
                return HttpNotFound();
            }
            return View(investor);
        }

        // POST: Investors/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            try
            {
                Investor investor = db.Investors.Find(id);
                db.Investors.Remove(investor);
                db.SaveChanges();
            }
            catch (Exception e)
            {
                return RedirectToAction("Index");
            }
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
