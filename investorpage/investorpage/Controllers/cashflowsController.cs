﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using investorpage.DAL;
using investorpage.Models;

namespace investorpage.Controllers
{
    public class cashflowsController : Controller
    {
        private InvestorContext db = new InvestorContext();

        // GET: cashflows
        public async Task<ActionResult> Index()
        {
            return View(await db.cashflowss.ToListAsync());
        }

        // GET: cashflows/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            cashflows cashflows = await db.cashflowss.FindAsync(id);
            if (cashflows == null)
            {
                return HttpNotFound();
            }
            return View(cashflows);
        }

        // GET: cashflows/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: cashflows/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "cashflowsID,increaseincash,netcash,PeriodStartDate,PeriodEndDate,year")] cashflows cashflows)
        {
            if (ModelState.IsValid)
            {
                db.cashflowss.Add(cashflows);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(cashflows);
        }

        // GET: cashflows/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            cashflows cashflows = await db.cashflowss.FindAsync(id);
            if (cashflows == null)
            {
                return HttpNotFound();
            }
            return View(cashflows);
        }

        // POST: cashflows/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "cashflowsID,increaseincash,netcash,PeriodStartDate,PeriodEndDate,year")] cashflows cashflows)
        {
            if (ModelState.IsValid)
            {
                db.Entry(cashflows).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(cashflows);
        }

        // GET: cashflows/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            cashflows cashflows = await db.cashflowss.FindAsync(id);
            if (cashflows == null)
            {
                return HttpNotFound();
            }
            return View(cashflows);
        }

        // POST: cashflows/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            cashflows cashflows = await db.cashflowss.FindAsync(id);
            db.cashflowss.Remove(cashflows);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
