﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using investorpage.DAL;
using investorpage.DAL.security;
using investorpage.Models;
using PagedList;
using WebMatrix.WebData;

namespace investorpage.Controllers
{
    [CustomAuthorize(Roles = "Secretary")]
    public class SecDividendController : Controller
    {
        private InvestorContext db = new InvestorContext();

        // GET: SecDividend
        public async Task<ActionResult> Index(string sortOrder, string currentFilter, string searchString, string Date1String, string Date2String,
            int? page)
        {
            ViewBag.CurrentSort = sortOrder;
            ViewBag.NameSortParm = String.IsNullOrEmpty(sortOrder) ? "name_desc" : "";
       
            ViewBag.DateSortParm = sortOrder == "Date" ? "date_desc" : "Date";
         
            var dividends = db.Dividends.Include(d => d.Investor).Include(d => d.User2);
            if (searchString != null || (Date1String != null && Date2String != null))
            {
                page = 1;
            }
            else
            {
                searchString = currentFilter;
            }
            ViewBag.CurrentFilter = searchString;
            if (!String.IsNullOrEmpty(Date1String) && !String.IsNullOrEmpty(Date2String))
            {
                DateTime date = Convert.ToDateTime(Date1String);
                DateTime date2 = Convert.ToDateTime(Date2String);
                dividends = from s in db.Dividends.Include(d => d.Investor).Include(d => d.User2)
                            where date <= s.AnnouncementDate && date2 >= s.AnnouncementDate
                                 select s;


            }
            switch (sortOrder)
            {
                case "name_desc":
                    dividends = dividends.OrderByDescending(s => s.DividendID);
                    break;
                case "Date":
                    dividends = dividends.OrderBy(s => s.AnnouncementDate);
                    break;
                case "date_desc":
                    dividends = dividends.OrderByDescending(s => s.AnnouncementDate);
                    break;
                default:
                    dividends = dividends.OrderBy(s => s.DividendID);
                    break;
            }
            int pageSize = 5;
            int pageNumber = (page ?? 1);

            return View(dividends.ToPagedList(pageNumber, pageSize));
        }

        // GET: SecDividend/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Dividend dividend = await db.Dividends.FindAsync(id);
            if (dividend == null)
            {
                return HttpNotFound();
            }
            return View(dividend);
        }

        // GET: SecDividend/Create
        public ActionResult Create()
        {
            ViewBag.InvestorID = new SelectList(db.Investors, "InvestorID", "FullName");
            string name = WebSecurity.CurrentUserName;
            //to select the user who is currently logged in
            var user = from U in db.Users2

                       where U.Email == name
                       select U;
      
            ViewBag.User2ID = new SelectList(user, "User2ID", "Username");
            return View();
        }

        // POST: SecDividend/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "DividendID,DividendType,AnnouncementDate,ExDividendDate,RecordDate,PaymentDate,DividendAmount,User2ID,InvestorID")] Dividend dividend)
        {
            if (ModelState.IsValid)
            {
                db.Dividends.Add(dividend);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            ViewBag.InvestorID = new SelectList(db.Investors, "InvestorID", "FullName", dividend.InvestorID);
            string name = WebSecurity.CurrentUserName;
            //to select the user who is currently logged in
            var user = from U in db.Users2

                       where U.Email == name
                       select U;
            ViewBag.User2ID = new SelectList(user, "User2ID", "Username", dividend.User2ID);
            return View(dividend);
        }

        // GET: SecDividend/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Dividend dividend = await db.Dividends.FindAsync(id);
            if (dividend == null)
            {
                return HttpNotFound();
            }
            ViewBag.InvestorID = new SelectList(db.Investors, "InvestorID", "FullName", dividend.InvestorID);
            ViewBag.User2ID = new SelectList(db.Users2, "User2ID", "Username", dividend.User2ID);
            return View(dividend);
        }

        // POST: SecDividend/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "DividendID,DividendType,AnnouncementDate,ExDividendDate,RecordDate,PaymentDate,DividendAmount,User2ID,InvestorID")] Dividend dividend)
        {
            if (ModelState.IsValid)
            {
                db.Entry(dividend).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.InvestorID = new SelectList(db.Investors, "InvestorID", "FullName", dividend.InvestorID);

            string name = WebSecurity.CurrentUserName;
            //to select the user who is currently logged in
            var user = from U in db.Users2

                       where U.Email == name
                       select U; ViewBag.User2ID = new SelectList(user, "User2ID", "Username", dividend.User2ID);
            return View(dividend);
        }

        // GET: SecDividend/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Dividend dividend = await db.Dividends.FindAsync(id);
            if (dividend == null)
            {
                return HttpNotFound();
            }
            return View(dividend);
        }

        // POST: SecDividend/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            Dividend dividend = await db.Dividends.FindAsync(id);
            db.Dividends.Remove(dividend);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
