﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using investorpage.DAL;
using investorpage.Models;

namespace investorpage.Controllers
{
    public class ordinarysharesController : Controller
    {
        private InvestorContext db = new InvestorContext();

        // GET: ordinaryshares
        public async Task<ActionResult> Index()
        {
            return View(await db.ordinaryshares.ToListAsync());
        }

        // GET: ordinaryshares/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ordinaryshare ordinaryshare = await db.ordinaryshares.FindAsync(id);
            if (ordinaryshare == null)
            {
                return HttpNotFound();
            }
            return View(ordinaryshare);
        }

        // GET: ordinaryshares/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: ordinaryshares/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "ordinaryshareID,basic_diluted,loss_per_share,market_price,shares_in_issue,PeriodStartDate,PeriodEndDate,year")] ordinaryshare ordinaryshare)
        {
            if (ModelState.IsValid)
            {
                db.ordinaryshares.Add(ordinaryshare);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(ordinaryshare);
        }

        // GET: ordinaryshares/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ordinaryshare ordinaryshare = await db.ordinaryshares.FindAsync(id);
            if (ordinaryshare == null)
            {
                return HttpNotFound();
            }
            return View(ordinaryshare);
        }

        // POST: ordinaryshares/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "ordinaryshareID,basic_diluted,loss_per_share,market_price,shares_in_issue,PeriodStartDate,PeriodEndDate,year")] ordinaryshare ordinaryshare)
        {
            if (ModelState.IsValid)
            {
                db.Entry(ordinaryshare).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(ordinaryshare);
        }

        // GET: ordinaryshares/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ordinaryshare ordinaryshare = await db.ordinaryshares.FindAsync(id);
            if (ordinaryshare == null)
            {
                return HttpNotFound();
            }
            return View(ordinaryshare);
        }

        // POST: ordinaryshares/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            ordinaryshare ordinaryshare = await db.ordinaryshares.FindAsync(id);
            db.ordinaryshares.Remove(ordinaryshare);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
