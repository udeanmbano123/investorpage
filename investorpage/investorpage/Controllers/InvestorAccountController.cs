﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using investorpage.DAL;
using investorpage.DAL.security;
using investorpage.Models;
using WebMatrix.WebData;

namespace investorpage.Controllers
{
    [CustomAuthorize(Roles = "User", NotifyUrl = "/UnauthorizedPage")]
    public class InvestorAccountController : Controller
    {
        private InvestorContext db = new InvestorContext();

        // GET: InvestorAccount
        public async Task<ActionResult> Index()
        {
            string name = WebSecurity.CurrentUserName;
            //to select the user who is currently logged in
            var user = from U in db.Users
                       join E in db.Investors on U.InvestorID equals E.InvestorID
                       where U.Email == name
                       select U;
            int? Logg = 0;
            //To assign logg to the value of the foreign key EmpID in table in User
            foreach (var key in user)
            {
                Logg = key.InvestorID;
            }

            var investor = from I in db.Investors
                where I.InvestorID == Logg
                select I;
            return View(await investor.ToListAsync());
        }

        // GET: InvestorAccount/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Investor investor = await db.Investors.FindAsync(id);
            if (investor == null)
            {
                return HttpNotFound();
            }
            return View(investor);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
