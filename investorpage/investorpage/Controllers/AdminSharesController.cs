﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using investorpage.DAL;
using investorpage.Models;

namespace investorpage.Controllers
{
    public class AdminSharesController : Controller
    {
        private InvestorContext db = new InvestorContext();

        // GET: AdminShares
        public async Task<ActionResult> Index()
        {
            var shares = db.Shares.Include(s => s.Administrator);
            return View(await shares.ToListAsync());
        }

        // GET: AdminShares/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Share share = await db.Shares.FindAsync(id);
            if (share == null)
            {
                return HttpNotFound();
            }
            return View(share);
        }

        // GET: AdminShares/Create
        public ActionResult Create()
        {
            ViewBag.AdministratorID = new SelectList(db.Administrator, "AdministratorID", "FirstName");
            return View();
        }

        // POST: AdminShares/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "ShareID,ShareName,ISIN,TradingStatus,Index,Sector,YearEnd,LastTrade,changeonday,ChiefExecutive,AdministratorID")] Share share)
        {
            if (ModelState.IsValid)
            {
                db.Shares.Add(share);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            ViewBag.AdministratorID = new SelectList(db.Administrator, "AdministratorID", "FirstName", share.AdministratorID);
            return View(share);
        }

        // GET: AdminShares/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Share share = await db.Shares.FindAsync(id);
            if (share == null)
            {
                return HttpNotFound();
            }
            ViewBag.AdministratorID = new SelectList(db.Administrator, "AdministratorID", "FirstName", share.AdministratorID);
            return View(share);
        }

        // POST: AdminShares/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "ShareID,ShareName,ISIN,TradingStatus,Index,Sector,YearEnd,LastTrade,changeonday,ChiefExecutive,AdministratorID")] Share share)
        {
            if (ModelState.IsValid)
            {
                db.Entry(share).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.AdministratorID = new SelectList(db.Administrator, "AdministratorID", "FirstName", share.AdministratorID);
            return View(share);
        }

        // GET: AdminShares/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Share share = await db.Shares.FindAsync(id);
            if (share == null)
            {
                return HttpNotFound();
            }
            return View(share);
        }

        // POST: AdminShares/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            Share share = await db.Shares.FindAsync(id);
            db.Shares.Remove(share);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
