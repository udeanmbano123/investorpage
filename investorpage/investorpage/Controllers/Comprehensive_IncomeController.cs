﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using investorpage.DAL;
using investorpage.Models;

namespace investorpage.Controllers
{
    public class Comprehensive_IncomeController : Controller
    {
        private InvestorContext db = new InvestorContext();

        // GET: Comprehensive_Income
        public async Task<ActionResult> Index()
        {
            return View(await db.ComprehensiveIncomes.ToListAsync());
        }

        // GET: Comprehensive_Income/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Comprehensive_Income comprehensive_Income = await db.ComprehensiveIncomes.FindAsync(id);
            if (comprehensive_Income == null)
            {
                return HttpNotFound();
            }
            return View(comprehensive_Income);
        }

        // GET: Comprehensive_Income/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Comprehensive_Income/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "Comprehensive_IncomeID,Revenue,operatingprofit,netinterest,lossorprofit,PeriodStartDate,PeriodEndDate,year")] Comprehensive_Income comprehensive_Income)
        {
            if (ModelState.IsValid)
            {
                db.ComprehensiveIncomes.Add(comprehensive_Income);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(comprehensive_Income);
        }

        // GET: Comprehensive_Income/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Comprehensive_Income comprehensive_Income = await db.ComprehensiveIncomes.FindAsync(id);
            if (comprehensive_Income == null)
            {
                return HttpNotFound();
            }
            return View(comprehensive_Income);
        }

        // POST: Comprehensive_Income/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Comprehensive_IncomeID,Revenue,operatingprofit,netinterest,lossorprofit,PeriodStartDate,PeriodEndDate,year")] Comprehensive_Income comprehensive_Income)
        {
            if (ModelState.IsValid)
            {
                db.Entry(comprehensive_Income).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(comprehensive_Income);
        }

        // GET: Comprehensive_Income/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Comprehensive_Income comprehensive_Income = await db.ComprehensiveIncomes.FindAsync(id);
            if (comprehensive_Income == null)
            {
                return HttpNotFound();
            }
            return View(comprehensive_Income);
        }

        // POST: Comprehensive_Income/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            Comprehensive_Income comprehensive_Income = await db.ComprehensiveIncomes.FindAsync(id);
            db.ComprehensiveIncomes.Remove(comprehensive_Income);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
