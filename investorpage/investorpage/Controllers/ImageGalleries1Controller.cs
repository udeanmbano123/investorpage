﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using investorpage.DAL;
using investorpage.Models;

namespace investorpage.Controllers
{
    public class ImageGalleries1Controller : Controller
    {
        private InvestorContext db = new InvestorContext();

        // GET: ImageGalleries1
        public async Task<ActionResult> Index()
        {
            return View(await db.ImageGallery.ToListAsync());
        }

        // GET: ImageGalleries1/Details/5
        public async Task<ActionResult> Details(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ImageGallery imageGallery = await db.ImageGallery.FindAsync(id);
            if (imageGallery == null)
            {
                return HttpNotFound();
            }
            return View(imageGallery);
        }

 
        // POST: ImageGalleries1/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
  
        // GET: ImageGalleries1/Edit/5


        // GET: ImageGalleries1/Delete/5
        public async Task<ActionResult> Delete(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ImageGallery imageGallery = await db.ImageGallery.FindAsync(id);
            if (imageGallery == null)
            {
                return HttpNotFound();
            }
            return View(imageGallery);
        }

        // POST: ImageGalleries1/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(Guid id)
        {
            ImageGallery imageGallery = await db.ImageGallery.FindAsync(id);
            db.ImageGallery.Remove(imageGallery);
            string fullPath = Request.MapPath("~/Upload_Files/" + imageGallery.Name);
            if (System.IO.File.Exists(fullPath))
            {
                System.IO.File.Delete(fullPath);
            }
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
