﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using investorpage.DAL;
using investorpage.Models;

namespace investorpage.Controllers
{
    public class financial_positionController : Controller
    {
        private InvestorContext db = new InvestorContext();

        // GET: financial_position
        public async Task<ActionResult> Index()
        {
            return View(await db.financial_postions.ToListAsync());
        }

        // GET: financial_position/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            financial_position financial_position = await db.financial_postions.FindAsync(id);
            if (financial_position == null)
            {
                return HttpNotFound();
            }
            return View(financial_position);
        }

        // GET: financial_position/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: financial_position/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "financial_positionID,equityattributable,interestdebt,PeriodStartDate,PeriodEndDate,year")] financial_position financial_position)
        {
            if (ModelState.IsValid)
            {
                db.financial_postions.Add(financial_position);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(financial_position);
        }

        // GET: financial_position/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            financial_position financial_position = await db.financial_postions.FindAsync(id);
            if (financial_position == null)
            {
                return HttpNotFound();
            }
            return View(financial_position);
        }

        // POST: financial_position/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "financial_positionID,equityattributable,interestdebt,PeriodStartDate,PeriodEndDate,year")] financial_position financial_position)
        {
            if (ModelState.IsValid)
            {
                db.Entry(financial_position).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(financial_position);
        }

        // GET: financial_position/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            financial_position financial_position = await db.financial_postions.FindAsync(id);
            if (financial_position == null)
            {
                return HttpNotFound();
            }
            return View(financial_position);
        }

        // POST: financial_position/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            financial_position financial_position = await db.financial_postions.FindAsync(id);
            db.financial_postions.Remove(financial_position);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
