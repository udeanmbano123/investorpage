﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Net.Mail;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.Mvc;
using investorpage.DAL;
using investorpage.Models;

namespace investorpage.Controllers
{
    public class SignUpController : Controller
    {
        private InvestorContext db = new InvestorContext();

   
  

        // GET: SignUp/Create
        public ActionResult Create()
        {
            var investors = (from tr in db.Investors
                             where !db.Users.Any(dm => tr.InvestorID == dm.InvestorID )
                             select tr).ToList();

            ViewBag.InvestorID = new SelectList(investors, "InvestorID", "FullName");
            return View();
        }

        // POST: SignUp/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "UserID,Username,Email,Password,ConfirmPassword,FirstName,LastName,InvestorID,IsActive,CreateDate")] User user)
        {
            // this
            var model = new User();
            if (ModelState.IsValid)
            {
                try
                {
                    user.IsActive=true;
                user.CreateDate = DateTime.Now;
                //Autogenerate password
                string allowedChars = "";
                allowedChars = "a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z,";
                allowedChars += "A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z,";
                allowedChars += "1,2,3,4,5,6,7,8,9,0,!,@,#,$,%,&,?";
                char[] sep = { ',' };
                string[] arr = allowedChars.Split(sep);
                string passwordString = "";
                string password = "";
                string temp = "";
                Random rand = new Random();
                for (int i = 0; i < 14; i++)
                {
                    temp = arr[rand.Next(0, arr.Length)];
                    passwordString += temp;
                }
                    password = passwordString;
                string hPassword = ComputeHash(password, new SHA256CryptoServiceProvider());
                    user.Password = hPassword;
                    user.ConfirmPassword = hPassword;
                    Role role2 = new Role { RoleName = "User" };
                user.Roles = new List<Role>();
                user.Roles.Add(role2);
                db.Users.Add(user);
              


                    await db.SaveChangesAsync();


                    //email
                    //email
                 
                    string messageBody = "Username :  " + Request["Email"] + "Password:" + password;
                    var body = "<p>Email From: {0} ({1})</p><p>Message:</p><p>{2}</p>";
                    var message = new MailMessage();
                    message.To.Add(new MailAddress(Request["Email"])); // replace with valid value 
                    message.From = new MailAddress("sender@outlook.com"); // replace with valid value
                    message.Subject = "Investor Portal Login Credentials";
                    message.Body = string.Format(body, "Admin", "udeanmbano@gmail.com", messageBody);
                    message.IsBodyHtml = true;

                    using (var smtp = new SmtpClient())
                    {
                        var credential = new NetworkCredential
                        {
                            UserName = "udeanmbano@gmail.com", // replace with valid value
                            Password = "icomefromzimbabwe" // replace with valid value
                        };
                        smtp.Credentials = credential;
                        smtp.Host = "smtp.gmail.com";
                        smtp.Port = 587;
                        smtp.EnableSsl = true;
                        await smtp.SendMailAsync(message);

                    }
                }
                catch (Exception e)
                {


                    var mod = ModelState.First(c => c.Key == "Email");  // this
                    mod.Value.Errors.Add("Duplicate emails  are not allowed");    // this

                    var investors2 = (from tr in db.Investors
                                     where !db.Users.Any(dm => tr.InvestorID == dm.InvestorID)
                                     select tr).ToList();
                    ViewBag.InvestorID = new SelectList(investors2, "InvestorID", "FullName", user.InvestorID);
                    return View(user);
                }



                return RedirectToAction("Index", "Account");
            }
            var investors = (from tr in db.Investors
                             where !db.Users.Any(dm => tr.InvestorID == dm.InvestorID)
                             select tr).ToList();
            ViewBag.InvestorID = new SelectList(investors, "InvestorID", "InvestorType", user.InvestorID);
            return View(user);
        }

        // GET: SignUp/Edit/5
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
        //password  hashing function
        public string ComputeHash(string input, HashAlgorithm algorithm)
        {
            Byte[] inputBytes = Encoding.UTF8.GetBytes(input);

            Byte[] hashedBytes = algorithm.ComputeHash(inputBytes);

            return BitConverter.ToString(hashedBytes);
        }
    }
}
