﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Web.Services;
public partial class _Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
    }
   
    [WebMethod]
    public static List<Data> GetData()
    {
        SqlConnection conn = new SqlConnection("Data Source =.; Initial Catalog = InvestorPageDB; Integrated Security = SSPI");
        DataSet ds = new DataSet();
        DataTable dt = new DataTable();
        conn.Open();
        string cmdstr = "select  year, SUM(Revenue) As Total,SUM(netinterest) As Interest,SUM(operatingprofit) As Operating from Comprehensive_Income group by year";
        SqlCommand cmd = new SqlCommand(cmdstr, conn);
        SqlDataAdapter adp = new SqlDataAdapter(cmd);
        adp.Fill(ds);dt = ds.Tables[0];List<Data> dataList = new List<Data>();
        string cat="";
        int val=0;
        long val2 = 0;
        long val3 = 0;
        foreach (DataRow dr in dt.Rows)
        {
            cat=dr[0].ToString();
            val =Convert.ToInt32( dr[1]);
            val2 = Convert.ToInt64(dr[2]);
            val3 = Convert.ToInt64(dr[3]);
            dataList.Add(new Data(cat, val,val2,val3));
        }
        return dataList;
    }
}
public class Data{
    public string ColumnName = "";
    public int Value = 0;
    public long Value2;
    public long Value3;
    public Data(string columnName, int value,long value2, long value3)
    {
        ColumnName = columnName;
        Value = value;
        Value2 = value2;
        Value3 = value3;
    }
}    
  