﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.0/jquery.min.js" type="text/javascript"></script>
  <script type="text/javascript" src="//www.google.com/jsapi"></script>
    <script type="text/javascript">
        google.load('visualization', '1', { packages: ['corechart'] });
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            $.ajax({
                type: 'POST',
                dataType: 'json',
                contentType: 'application/json',
                url: 'Default.aspx/GetData',
                data: '{}',
                success:
                    function (response) {
                        drawVisualization(response.d);
                    }
                });
        })

        function drawVisualization(dataValues) {
            var data = new google.visualization.DataTable();
            data.addColumn('string', 'Year');
            data.addColumn('number', 'Revenue');
            data.addColumn('number', 'Net Interest');
            data.addColumn('number', 'Operating Profit');
          
            for (var i = 0; i < dataValues.length; i++) {
                data.addRow([dataValues[i].ColumnName, dataValues[i].Value, dataValues[i].Value2, dataValues[i].Value3]);
            }

            var materialOptions = {
                title: 'Investor  Financial Trends',
                backgroundColor: 'black',
                is3D: true,
              
                width: 900,
                height: 500,
                titleTextStyle: {
                    color: 'white'
                },
                hAxis: {
                    textStyle: {
                        color: 'white'
                    },
                    titleTextStyle: {
                        color: 'white'
                    }
                },
                vAxis: {
                    textStyle: {
                        color: 'white'
                    },
                    titleTextStyle: {
                        color: 'white'
                    }
                },
                legend: {
                    textStyle: {
                        color: 'white'
                    }
                }
            };
           

            new google.visualization.LineChart(document.getElementById('visualization')).draw(data, materialOptions);
        } 
         
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div id="visualization" style="width: 600px; height: 350px;">
    </div>
    </form>
</body>
</html>
