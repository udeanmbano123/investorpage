﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace investorpage
{
    public partial class ColumnChart : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        [WebMethod]
        public static List<ChartDetails> GetColumnChartData()
        {
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["InvestorContext"].ToString()))
            {
                SqlCommand cmd = new SqlCommand("Usp_Getdata", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = cmd;
                DataTable dt = new DataTable();
                da.Fill(dt);

                List<ChartDetails> dataList = new List<ChartDetails>();

                foreach (DataRow dtrow in dt.Rows)
                {
                    ChartDetails details = new ChartDetails();
                    details.year = dtrow[0].ToString();
                    details.Revenue = Convert.ToInt32(dtrow[1]);

                    dataList.Add(details);
                }
                return dataList;
            }
        }
        public class ChartDetails
        {
            public string year { get; set; }
            public int Revenue { get; set; }
        }
    }
}